import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import Vue2Filters from 'vue2-filters'
import Vuelidate from 'vuelidate'
import VueTheMask from 'vue-the-mask'

Vue.use(Vue2Filters)

import firebase from 'firebase/app'
import 'firebase/auth'
import 'firebase/database'

Vue.use(
  Vuelidate,
  VueTheMask
)

Vue.config.productionTip = false

firebase.initializeApp({
  apiKey: "AIzaSyCJnvKTzYbEUhS8Zj32ua7x6jMeSU8X-xM",
  authDomain: "payment-terminal-5cd5d.firebaseapp.com",
  databaseURL: "https://payment-terminal-5cd5d.firebaseio.com",
  projectId: "payment-terminal-5cd5d",
  storageBucket: "payment-terminal-5cd5d.appspot.com",
  messagingSenderId: "289115057670",
  appId: "1:289115057670:web:8586aba93467fd30069d87",
  measurementId: "G-TN3GJSZ2QL"
})

export const eBus = new Vue();

let app

firebase.auth().onAuthStateChanged(() =>{
  if(!app){
    app = new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')
  }
})


