import firebase from 'firebase/app'

export default {
    state: {
        userInfo: null
    },
    mutations:{
        SET_USERINFO(state, payload){
            state.userInfo = payload
        },
        CLEAR_USERINFO(state){
            state.userInfo = {}
        }
    },
    actions: {
        async login(context, payload){
            try {
                await firebase.auth().signInWithEmailAndPassword(payload.email, payload.password)
            } catch (error) {
                console.log(error);
                context.commit('SET_ERROR', error.message)
                throw error
            }
        },
        async logout(context){
            await firebase.auth().signOut()
            context.commit('CLEAR_USERINFO')
        },
        async getUser(context){
            const user = await firebase.auth().currentUser
            context.commit('SET_USERINFO', user)
        },
        getUid(){
            const user = firebase.auth().currentUser
            return user ? user.uid : null
        }
    },
    getters: {
        userInfoGet: s => s.userInfo
    }
}