import Vue from 'vue'
import Vuex from 'vuex'
import auth from './auth'
import operators from './operators'

Vue.use(Vuex)

export default new Vuex.Store({
  state: {
    error: null,
    process: false,
    alertMessage: {
      message: '',
      status: false
    },
    popupMessage:{
      message: '',
      status: false
    }
  },
  mutations: {
    SET_ERROR(state, payload){
      state.error = payload
    },
    CLEAR_ERROR(state){
      state.error = ''
    },
    SET_PROCESS(state, payload){
      state.process = payload
    },
    SET_ALERTMESSAGE(state, payload){
      state.alertMessage = payload;
    },
    SET_POPUPMESSAGE(state, payload){
      state.popupMessage = payload;
    }
  },
  actions: {
  },
  getters:{
    process: s => s.process,
    alertMessage: s => s.alertMessage,
    popupMessage: s => s.popupMessage
  },
  modules: {
    auth,
    operators
  }
})
