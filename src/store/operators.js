import firebase from 'firebase/app'
export default {
    state: {
        operators: null,
        operatorByName: null
    },
    mutations:{
        SET_OPERATORS(state, payload){
            state.operators = payload;
        },
        SET_CURRENTBALANCE(state, payload){
            state.currentBalance = payload;
        },
        SET_OPERATORBYNAME(state, payload){
            state.operatorByName = payload;
        },
        RESET_OPERATORBYNAME(state, payload){
            state.operatorByName = payload;
        }
    },
    actions: {
        async getOpertaorsList (context){
            const uid = await context.dispatch('getUid');
            if (uid) {
                const operators = await firebase.database().ref(`/users/${uid}/operators`).once('value');
                const operatorsValue = operators.val();
                console.log(operatorsValue);
                
                context.commit('SET_OPERATORS', operatorsValue);
            }
        },
        async setNewBalanceInfo (context, payload){
            const uid = await context.dispatch('getUid');
            if(uid){
                await firebase.database().ref(`users/${uid}/operators/${payload.routerName}`).set(payload);
            }
        },
        async getOperatorListByName(context, payload){
            const uid = await context.dispatch('getUid');
            const operatorByName = await firebase.database().ref(`users/${uid}/operators/${payload}`).once('value');
            const operatorByNameValue = operatorByName.val();
            console.log(operatorByNameValue);
            context.commit('SET_OPERATORBYNAME', operatorByNameValue);
            
        }
    },
    getters: {
        operatorsList: s => s.operators,
        operatorByName: s => s.operatorByName,
        // operatorActive: s => name => s.operators.find(oper => oper.name === name)
        // operatorActive: (state) => {
        //     return (name) => {
        //         for (const key in state.operators) {
        //             if (state.operators.hasOwnProperty(key)) {
        //                 const element = state.operators[key];
        //                 if(element.routerName === name){
        //                     return element
        //                 }
        //             }
        //         }
        //     }
        // }
    }
}