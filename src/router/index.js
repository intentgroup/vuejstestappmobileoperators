import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../views/Home.vue'

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    name: 'home',
    component: Home
  },
  {
    path: '/operators',
    name: 'operators-main',
    component: () => import('@/views/Operators.vue'),
    children:[
      {
        path: '/operators',
        name: 'operators-list',
        component: () => import('@/components/OperatorsList.vue')
      },
      {
        path: '/operators/:name',
        name: 'balance',
        component: () => import ('@/components/Balance.vue')
      }
    ]
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
